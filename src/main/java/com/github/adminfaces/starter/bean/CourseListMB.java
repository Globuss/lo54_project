/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.bean;

import com.github.adminfaces.starter.infra.model.Filter;
import com.github.adminfaces.starter.model.Course;
import com.github.adminfaces.starter.service.CourseService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;

/**
 *
 * @author Theo
 */
@Named
@ViewScoped
public class CourseListMB implements Serializable{
    
    @Inject
    CourseService courseService;
    
    List<Course> courses;
    
    Course selectedCourse;
    
    Filter<Course> filter = new Filter<>(new Course());

    List<Course> filteredValue;
    
    @PostConstruct
    public void initDataModel() {
        this.courses = this.courseService.getAllCourses();
    }
    
    public void clear() {
        filter = new Filter<Course>(new Course());
    }
    
    public void delete(){
        
    }

    public CourseService getCourseService() {
        return courseService;
    }

    public void setCourseService(CourseService courseService) {
        this.courseService = courseService;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public Course getSelectedCourse() {
        return selectedCourse;
    }

    public void setSelectedCourse(Course selectedCourse) {
        this.selectedCourse = selectedCourse;
    }

    public Filter<Course> getFilter() {
        return filter;
    }

    public void setFilter(Filter<Course> filter) {
        this.filter = filter;
    }

    public List<Course> getFilteredValue() {
        return filteredValue;
    }

    public void setFilteredValue(List<Course> filteredValue) {
        this.filteredValue = filteredValue;
    }
    
    
    
}
