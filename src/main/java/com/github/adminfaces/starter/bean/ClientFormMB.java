/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.bean;

import com.github.adminfaces.starter.model.Client;
import com.github.adminfaces.starter.model.Location;
import com.github.adminfaces.starter.service.ClientService;
import static com.github.adminfaces.starter.util.Utils.addDetailMessage;
import static com.github.adminfaces.template.util.Assert.has;
import java.io.IOException;
import java.io.Serializable;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;

/**
 *
 * @author Theo
 */
@Named
@ViewScoped
public class ClientFormMB implements Serializable {
    
    private Integer id;
    private Client client;


    @Inject
    ClientService clientService;

    public void init() {
        if(Faces.isAjaxRequest()){
           return;
        }
        if (has(id)) {
            client = clientService.findById(id);
        } else {
            client = new Client();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public void remove() throws IOException {
        if (has(client) && has(client.getId())) {
            clientService.delete(client);
            addDetailMessage("Client " + client.getFirstname() + " " + client.getLastname()
                    + " correctement supprimé");
            Faces.getFlash().setKeepMessages(true);
            Faces.redirect("client-list.xhtml");
        }
    }

    public void save() {
        String msg;
        if (client.getId() == null) {
            clientService.create(client);
            msg = "Client " + client.getFirstname() + " " + client.getLastname() + " correctement créé";
        } else {
            clientService.update(client);
            msg = "Client " + client.getFirstname() + " " + client.getLastname() + " correctement mise à jour";
        }
        addDetailMessage(msg);
    }

    public void clear() {
        client = new Client();
        id = null;
    }

    public boolean isNew() {
        return client == null || client.getId() == null;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public ClientService getClientService() {
        return clientService;
    }

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }
    
    
}
