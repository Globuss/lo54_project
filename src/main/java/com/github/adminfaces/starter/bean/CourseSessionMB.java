/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.bean;

import com.github.adminfaces.starter.infra.model.Filter;
import com.github.adminfaces.starter.model.CourseSession;
import com.github.adminfaces.starter.service.CourseSessionService;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;

/**
 *
 * @author Theo
 */
@Named
@ViewScoped
public class CourseSessionMB implements Serializable {
    
    @Inject
    CourseSessionService courseSessionService;
    
    List<CourseSession> coursesSessions;
    
    List<CourseSession> coursesSessionsUpcoming;
    
    CourseSession selectedCourseSession;
    
    Filter<CourseSession> filter = new Filter<>(new CourseSession());

    List<CourseSession> filteredValue;
    
    @PostConstruct
    public void initDataModel() {
        this.coursesSessions = this.courseSessionService.getAllCoursesSessions();
        this.coursesSessionsUpcoming = this.courseSessionService.getAllCoursesSessionsUpcoming();
    }
    
    public void clear() {
        filter = new Filter<CourseSession>(new CourseSession());
    }
    
    public void delete(){
        
    }
    
    public boolean filterByDate(Object value, Object filter, Locale locale) {
        
        if( filter == null ) {
            return true;
        }

        if( value == null ) {
            return true;
        }
                
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String format = formatter.format(filter);
        
        System.out.println(value);
        System.out.println(format);
        
        return value.toString().indexOf(format) >= 0;
    }

    public List<CourseSession> getCoursesSessions() {
        return coursesSessions;
    }

    public void setCoursesSessions(List<CourseSession> coursesSessions) {
        this.coursesSessions = coursesSessions;
    }
    
    public CourseSession getSelectedCourseSession() {
        return selectedCourseSession;
    }

    public void setSelectedCourseSession(CourseSession selectedCourseSession) {
        this.selectedCourseSession = selectedCourseSession;
    }

    public CourseSessionService getCourseSessionService() {
        return courseSessionService;
    }

    public void setCourseSessionService(CourseSessionService courseSessionService) {
        this.courseSessionService = courseSessionService;
    }

    public Filter<CourseSession> getFilter() {
        return filter;
    }

    public void setFilter(Filter<CourseSession> filter) {
        this.filter = filter;
    }

    public List<CourseSession> getFilteredValue() {
        return filteredValue;
    }

    public void setFilteredValue(List<CourseSession> filteredValue) {
        this.filteredValue = filteredValue;
    }

    public List<CourseSession> getCoursesSessionsUpcoming() {
        return coursesSessionsUpcoming;
    }

    public void setCoursesSessionsUpcoming(List<CourseSession> coursesSessionsUpcoming) {
        this.coursesSessionsUpcoming = coursesSessionsUpcoming;
    }
    
    
    
}
