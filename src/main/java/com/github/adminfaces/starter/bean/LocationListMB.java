/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.bean;

import com.github.adminfaces.starter.infra.model.Filter;
import com.github.adminfaces.starter.model.CourseSession;
import com.github.adminfaces.starter.model.Location;
import com.github.adminfaces.starter.service.LocationService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;

/**
 *
 * @author Theo
 */
@Named
@ViewScoped
public class LocationListMB implements Serializable {
    
    @Inject
    LocationService locationService;
    
    List<Location> locations;
    
    Location selectedLocations;

    List<Location> filteredValue;
    
    Filter<Location> filter = new Filter<>(new Location());
    
    @PostConstruct
    public void initData(){
        this.locations = this.locationService.getAllLocations();
    }
    
    public void clear(){
        filter = new Filter<>(new Location());
    }

    public LocationService getLocationService() {
        return locationService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    public Location getSelectedLocations() {
        return selectedLocations;
    }

    public void setSelectedLocations(Location selectedLocations) {
        this.selectedLocations = selectedLocations;
    }

    public List<Location> getFilteredValue() {
        return filteredValue;
    }

    public void setFilteredValue(List<Location> filteredValue) {
        this.filteredValue = filteredValue;
    }
    
    
    
}
