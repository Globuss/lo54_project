/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.bean;

import com.github.adminfaces.starter.model.Client;
import com.github.adminfaces.starter.model.CourseSession;
import com.github.adminfaces.starter.service.CourseSessionService;
import static com.github.adminfaces.starter.util.Utils.addDetailMessage;
import static com.github.adminfaces.template.util.Assert.has;
import java.io.IOException;
import java.io.Serializable;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;

/**
 *
 * @author Theo
 */
@Named
@ViewScoped
public class CourseSessionFormMB implements Serializable {
    
    private Integer id;
    private CourseSession courseSession;


    @Inject
    CourseSessionService courseSessionService;

    public void init() {
        if(Faces.isAjaxRequest()){
           return;
        }
        if (has(id)) {
            courseSession = courseSessionService.findById(id);
        } else {
            courseSession = new CourseSession();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public void remove() throws IOException {
        if (has(courseSession) && has(courseSession.getId())) {
            courseSessionService.delete(courseSession);
            addDetailMessage("Session " + courseSession.getCourseCode().getCode()
                    + " correctement supprimé");
            Faces.getFlash().setKeepMessages(true);
            Faces.redirect("session-list.xhtml");
        }
    }

    public void save() {
        
        System.out.println(courseSession);
        
        String msg;
        if (courseSession.getId() == null) {
            courseSessionService.create(courseSession);
            msg = "Session " + courseSession.getCourseCode().getCode() + " correctement créé";
        } else {
            courseSessionService.update(courseSession);
            msg = "Session " + courseSession.getCourseCode().getCode() + " correctement mise à jour";
        }
        addDetailMessage(msg);
    }

    public void clear() {
        courseSession = new CourseSession();
        id = null;
    }

    public boolean isNew() {
        return courseSession == null || courseSession.getId() == null;
    }

    public CourseSession getCourseSession() {
        return courseSession;
    }

    public void setCourseSession(CourseSession courseSession) {
        this.courseSession = courseSession;
    }

    public CourseSessionService getCourseSessionService() {
        return courseSessionService;
    }

    public void setCourseSessionService(CourseSessionService courseSessionService) {
        this.courseSessionService = courseSessionService;
    }
    
}
