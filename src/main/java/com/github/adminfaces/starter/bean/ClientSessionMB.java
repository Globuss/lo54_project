/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.bean;

import com.github.adminfaces.starter.infra.model.Filter;
import com.github.adminfaces.starter.infra.security.LogonMB;
import com.github.adminfaces.starter.model.Client;
import com.github.adminfaces.starter.model.ClientSession;
import com.github.adminfaces.starter.model.CourseSession;
import com.github.adminfaces.starter.service.ClientService;
import com.github.adminfaces.starter.service.ClientSessionService;
import static com.github.adminfaces.starter.util.Utils.addDetailMessage;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Theo
 */
@Named
@ViewScoped
public class ClientSessionMB implements Serializable {
    
    @Inject
    ClientSessionService clientSessionService;
    
    @Inject
    LogonMB logonService;
    
    @Inject
    ClientService clientService;
    
    List<ClientSession> clientSesions;
    
    ClientSession selectedClientSession;
    
    Filter<ClientSession> filter = new Filter<>(new ClientSession());

    List<ClientSession> filteredValue;
    
    @PostConstruct
    public void initDataModel() {
        this.clientSesions = this.clientSessionService.getAllClientSessionByName(logonService.getCurrentUser());
    }
    
    public void clear() {
        filter = new Filter<ClientSession>(new ClientSession());
    }
    
    public void delete(){
        
    }
    
    public void add(CourseSession s) throws IOException{
        System.out.println(s);
        
        Client client = clientService.findByEmail(logonService.getCurrentUser());
        
        if(client != null){
            ClientSession clientSession = new ClientSession();
            clientSession.setClientId(client);
            clientSession.setSessionId(s);
            if(clientSessionService.create(clientSession)){
                initDataModel();
                System.out.println(this.clientSesions.size());
                RequestContext.getCurrentInstance().update("datatableSessions");
                System.out.println("Request");
                addDetailMessage("Inscription effectuée");
                Faces.redirect("index.xhtml");
            }else{
                addDetailMessage("Impossible de vous inscrire", FacesMessage.SEVERITY_ERROR);
            }
        }
        
    }
    
     public void remove(ClientSession s) throws IOException{
        System.out.println(s);
        
        Client client = clientService.findByEmail(logonService.getCurrentUser());
        
        if(client != null){
            ClientSessionService clientSessionService = new ClientSessionService();
            clientSessionService.delete(s);
            addDetailMessage("Désinscription effectuée");
            Faces.redirect("index.xhtml");
        }
        
    }
    
    public boolean filterByDate(Object value, Object filter, Locale locale) {
        
        if( filter == null ) {
            return true;
        }

        if( value == null ) {
            return true;
        }
                
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String format = formatter.format(filter);
        
        System.out.println(value);
        System.out.println(format);
        
        return value.toString().indexOf(format) >= 0;
    }

    public ClientSessionService getClientSessionService() {
        return clientSessionService;
    }

    public void setClientSessionService(ClientSessionService clientSessionService) {
        this.clientSessionService = clientSessionService;
    }

    public List<ClientSession> getClientSesions() {
        return clientSesions;
    }

    public void setClientSesions(List<ClientSession> clientSesions) {
        this.clientSesions = clientSesions;
    }

    public ClientSession getSelectedClientSession() {
        return selectedClientSession;
    }

    public void setSelectedClientSession(ClientSession selectedClientSession) {
        this.selectedClientSession = selectedClientSession;
    }

    public Filter<ClientSession> getFilter() {
        return filter;
    }

    public void setFilter(Filter<ClientSession> filter) {
        this.filter = filter;
    }

    public List<ClientSession> getFilteredValue() {
        return filteredValue;
    }

    public void setFilteredValue(List<ClientSession> filteredValue) {
        this.filteredValue = filteredValue;
    }
    
    
    
}
