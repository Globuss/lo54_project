/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.bean;

import com.github.adminfaces.starter.model.Location;
import com.github.adminfaces.starter.service.LocationService;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

import static com.github.adminfaces.starter.util.Utils.addDetailMessage;
import static com.github.adminfaces.template.util.Assert.has;

/**
 * @author Theo
 */
@Named
@ViewScoped
public class LocationFormMB implements Serializable {


    private Integer id;
    private Location location;
    
    @Inject
    LocationService locationService;

    public void init() {
        if(Faces.isAjaxRequest()){
           return;
        }
        if (has(id)) {
            location = locationService.findById(id);
        } else {
            location = new Location();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public void remove() throws IOException {
        if (has(location) && has(location.getId())) {
            locationService.delete(location);
            addDetailMessage("Ville " + location.getCity()
                    + " correctement supprimée");
            Faces.getFlash().setKeepMessages(true);
            Faces.redirect("city-list.xhtml");
        }
    }

    public void save() {
        String msg;
        if (location.getId() == null) {
            locationService.create(location);
            msg = "Ville " + location.getCity() + " correctement créée";
        } else {
            locationService.update(location);
            msg = "Ville " + location.getCity() + " correctement mise à jour";
        }
        addDetailMessage(msg);
    }

    public void clear() {
        location = new Location();
        id = null;
    }

    public boolean isNew() {
        return location == null || location.getId() == null;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public LocationService getLocationService() {
        return locationService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }


}
