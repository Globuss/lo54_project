/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.bean;
import com.github.adminfaces.starter.infra.model.Filter;
import com.github.adminfaces.starter.model.Client;
import com.github.adminfaces.starter.service.ClientService;
import org.omnifaces.cdi.ViewScoped;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

import java.util.List;
import javax.annotation.PostConstruct;


/**
 *
 * @author Theo
 */
@Named
@ViewScoped
public class ClientListMB implements Serializable {
    
    private Client client;
    
    @Inject
    ClientService clientService;
    
    List<Client> clients;
    
    Client selectedClient;
    
    Filter<Client> filter = new Filter<>(new Client());

    List<Client> filteredValue;
    
    @PostConstruct
    public void initDataModel() {
        this.clients = this.clientService.getAllClients();
    }
    
    public void createClient(){
        Client c = new Client(null, "Test", "Test", "13 rue test", "0354849412", "test@mail.com", "test", "testmdp");
        System.out.println(c);
        clientService.create(c);
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public ClientService getClientService() {
        return clientService;
    }

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    public Client getSelectedClient() {
        return selectedClient;
    }

    public void setSelectedClient(Client selectedClient) {
        this.selectedClient = selectedClient;
    }

    public Filter<Client> getFilter() {
        return filter;
    }

    public void setFilter(Filter<Client> filter) {
        this.filter = filter;
    }

    public List<Client> getFilteredValue() {
        return filteredValue;
    }

    public void setFilteredValue(List<Client> filteredValue) {
        this.filteredValue = filteredValue;
    }
    
    
}
