/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.bean;

import com.github.adminfaces.starter.model.Client;
import com.github.adminfaces.starter.model.Course;
import com.github.adminfaces.starter.service.CourseService;
import static com.github.adminfaces.starter.util.Utils.addDetailMessage;
import static com.github.adminfaces.template.util.Assert.has;
import java.io.IOException;
import java.io.Serializable;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;

/**
 *
 * @author Theo
 */
@Named
@ViewScoped
public class CourseFormMB implements Serializable {
     
    private Integer id;
    private Course course;


    @Inject
    CourseService courseService;

    public void init() {
        if(Faces.isAjaxRequest()){
           return;
        }
        if (has(id)) {
            course = courseService.findById(id);
        } else {
            course = new Course();
        }
    }

    public void remove() throws IOException {
        if (has(course) && has(course.getId())) {
            courseService.delete(course);
            addDetailMessage("Cours " + course.getCode()
                    + " correctement supprimé");
            Faces.getFlash().setKeepMessages(true);
            Faces.redirect("client-list.xhtml");
        }
    }

    public void save() {
        System.out.println(course);
        String msg;
        if (course.getId()== null) {
            courseService.create(course);
            msg = "Cours " + course.getCode() + " correctement créé";
        } else {
            courseService.update(course);
            msg = "Cours " + course.getCode() + " correctement mise à jour";
        }
        addDetailMessage(msg);
    }

    public void clear() {
        course = new Course();
        id = null;
    }

    public boolean isNew() {
        return course == null || course.getId()== null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public CourseService getCourseService() {
        return courseService;
    }

    public void setCourseService(CourseService courseService) {
        this.courseService = courseService;
    }
    
    
}
