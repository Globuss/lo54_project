package com.github.adminfaces.starter.util;

import com.github.adminfaces.starter.model.Client;
import com.github.adminfaces.starter.model.ClientSession;
import com.github.adminfaces.starter.model.Course;
import com.github.adminfaces.starter.model.CourseSession;
import com.github.adminfaces.starter.model.Location;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {

    //XML based configuration
    private static SessionFactory sessionFactory;

    //Annotation based configuration
    private static SessionFactory sessionAnnotationFactory;

    //Property based configuration
    private static SessionFactory sessionJavaConfigFactory;

    private static SessionFactory buildSessionFactory() {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            Configuration configuration = new Configuration();
            configuration.configure("hibernate.cfg.xml");
            System.out.println("Hibernate Configuration loaded");

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            System.out.println("Hibernate serviceRegistry created");

            configuration.addAnnotatedClass(Location.class);
            configuration.addAnnotatedClass(ClientSession.class);
            configuration.addAnnotatedClass(Client.class);
            configuration.addAnnotatedClass(Course.class);
            configuration.addAnnotatedClass(CourseSession.class);

            SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);

            return sessionFactory;
        }
        catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static SessionFactory getSessionFactory() {
        if(sessionFactory == null) sessionFactory = buildSessionFactory();
        System.out.println(sessionFactory);
        return sessionFactory;
    }

}
