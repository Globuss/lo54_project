/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Theo
 */
@Entity
@Table(name = "CLIENT_SESSION", catalog = "postgres", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClientSession.findAll", query = "SELECT c FROM ClientSession c")
    , @NamedQuery(name = "ClientSession.findById", query = "SELECT c FROM ClientSession c WHERE c.id = :id")})
public class ClientSession implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
    @JoinColumn(name = "CLIENT_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Client clientId;
    @JoinColumn(name = "SESSION_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private CourseSession sessionId;

    public ClientSession() {
    }

    public ClientSession(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Client getClientId() {
        return clientId;
    }

    public void setClientId(Client clientId) {
        this.clientId = clientId;
    }

    public CourseSession getSessionId() {
        return sessionId;
    }

    public void setSessionId(CourseSession sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClientSession)) {
            return false;
        }
        ClientSession other = (ClientSession) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.github.adminfaces.starter.model.ClientSession[ id=" + id + " ]";
    }
    
}
