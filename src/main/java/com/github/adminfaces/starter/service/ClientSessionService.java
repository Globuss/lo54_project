/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.service;

import com.github.adminfaces.starter.model.ClientSession;
import com.github.adminfaces.starter.model.Course;
import com.github.adminfaces.starter.util.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Theo
 */
@Stateless
public class ClientSessionService implements Serializable {
    
    public boolean create(ClientSession c) {
        boolean res = false;
        if(c.getSessionId().getClientSessionList().size() < c.getSessionId().getMax() &&
                !clientAlreadyInClientSession(c)){
            Transaction transaction = null;
            try (Session session = HibernateUtil.getSessionFactory().openSession()) {
                transaction = session.beginTransaction();

                session.save(c);

                transaction.commit();
                res = true;
            } catch (Exception e) {
                e.printStackTrace();
                if (transaction != null) {
                   transaction.rollback();
                }
            }
        }
        
        return res;
    }
    
    public boolean clientAlreadyInClientSession(ClientSession c){
        List<ClientSession> clientsSessions = null;
        
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            String hql = "FROM ClientSession c WHERE c.clientId.id = :clientId AND c.sessionId.id  = :sessionId";
            Query query = session.createQuery(hql);
            query.setInteger("clientId",c.getClientId().getId());
            query.setInteger("sessionId",c.getSessionId().getId());
            clientsSessions = query.list();
            
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
        
        return clientsSessions != null && clientsSessions.size() > 0;
    }
    
    public List<ClientSession> getAllCourses(){
        List<ClientSession> clientSessions = null;
        
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<ClientSession> query = builder.createQuery(ClientSession.class);
            Root<ClientSession> root = query.from(ClientSession.class);
            query.select(root);
            Query<ClientSession> q=session.createQuery(query);
            clientSessions=q.getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
        
        return clientSessions;
    }
    
    public List<ClientSession> getAllClientSessionByName(String userEmail){
        List<ClientSession> clientSessions = null;
        
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            String hql = "FROM ClientSession c WHERE c.clientId.email = :userEmail AND c.sessionId.startDate > CURRENT_DATE";
            Query query = session.createQuery(hql);
            query.setString("userEmail",userEmail);
            clientSessions = query.list();
            
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
        
        return clientSessions;
    }
    
    public ClientSession findById(int clientSessionId){
        ClientSession clientSession = null;
        
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            clientSession = session.get(ClientSession.class, clientSessionId);
            
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
        
        return clientSession;
    }
 
    public void delete(ClientSession c){
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            
            session.delete(c);

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
    }
    
    public void update(ClientSession c){
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            
            session.saveOrUpdate(c);

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
    }
}
