/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.service;

import com.github.adminfaces.starter.model.Course;
import com.github.adminfaces.starter.util.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Theo
 */
@Stateless
public class CourseService implements Serializable {
    
    public void create(Course c) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            session.save(c);
            
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
    }
    
    public List<Course> getAllCourses(){
        List<Course> courses = null;
        
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Course> query = builder.createQuery(Course.class);
            Root<Course> root = query.from(Course.class);
            query.select(root);
            Query<Course> q=session.createQuery(query);
            courses=q.getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
        
        return courses;
    }
    
    public Course findById(int courseId){
        Course course = null;
        
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            course = session.get(Course.class, courseId);
            
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
        
        return course;
    }
 
    public void delete(Course c){
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            
            session.delete(c);

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
    }
    
    public void update(Course c){
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            
            session.saveOrUpdate(c);

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
    }
    
}
