/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.service;

import com.github.adminfaces.starter.model.Client;
import com.github.adminfaces.starter.util.HibernateUtil;

import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Theo
 */
@Stateless
public class ClientService implements Serializable {
    
    public void create(Client c) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        session.save(c);
        
        session.getTransaction().commit();
        session.close();
    }
    
    public List<Client> getAllClients(){
        List<Client> clients = null;
        
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Client> query = builder.createQuery(Client.class);
            Root<Client> root = query.from(Client.class);
            query.select(root);
            Query<Client> q=session.createQuery(query);
            clients=q.getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
        
        return clients;
    }
    
    public Client findById(int clientId){
        Client client = null;
        
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            client = session.get(Client.class, clientId);
            
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
        
        return client;
    }
    
    public Client findByEmail(String clientEmail){
        Client client = null;
        
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            String hql = "FROM Client c WHERE c.email = :userEmail";
            Query query = session.createQuery(hql);
            query.setString("userEmail",clientEmail);
            client = (Client)query.getSingleResult();
            
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
        
        return client;
    }
        
    public void delete(Client c){
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            
            session.delete(c);

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
    }
    
    public void update(Client c){
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            
            session.saveOrUpdate(c);

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
    }
    
}
