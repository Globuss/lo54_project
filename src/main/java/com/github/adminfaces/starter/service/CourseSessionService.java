/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.service;

import com.github.adminfaces.starter.model.CourseSession;
import com.github.adminfaces.starter.util.HibernateUtil;

import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Theo
 */
@Stateless
public class CourseSessionService implements Serializable {
    
    public void create(CourseSession c) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            
            session.save(c);

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
    }
    
    public CourseSession findById(int courseSessionId){
        CourseSession courseSession = null;
        
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            courseSession = session.get(CourseSession.class, courseSessionId);
            
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
        
        return courseSession;
    }
    
    public void delete(CourseSession c){
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            
            session.delete(c);

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
    }
    
    public void update(CourseSession c){
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            
            session.saveOrUpdate(c);

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
    }
    
    public List<CourseSession> getAllCoursesSessions(){
        List<CourseSession> coursesSessions = null;
        
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<CourseSession> query = builder.createQuery(CourseSession.class);
            Root<CourseSession> root = query.from(CourseSession.class);
            query.select(root);
            Query<CourseSession> q=session.createQuery(query);
            coursesSessions=q.getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
        
        return coursesSessions;
    }
    
    public List<CourseSession> getAllCoursesSessionsUpcoming(){
        List<CourseSession> coursesSessions = null;
        
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            String hql = "FROM CourseSession c WHERE c.startDate > CURRENT_DATE";
            Query query = session.createQuery(hql);
            coursesSessions = query.list();
            
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
               transaction.rollback();
            }
        }
        
        return coursesSessions;
    }
    
}
