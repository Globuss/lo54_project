package com.github.adminfaces.starter.infra.security;

import com.github.adminfaces.starter.model.Client;
import com.github.adminfaces.starter.service.ClientService;
import com.github.adminfaces.template.session.AdminSession;
import org.omnifaces.util.Faces;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Specializes;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

import static com.github.adminfaces.starter.util.Utils.addDetailMessage;
import java.util.List;
import javax.faces.application.FacesMessage;
import org.omnifaces.util.Messages;

/**
 * Created by rmpestano on 12/20/14.
 *
 * This is just a login example.
 *
 * AdminSession uses isLoggedIn to determine if user must be redirect to login page or not.
 * By default AdminSession isLoggedIn always resolves to true so it will not try to redirect user.
 *
 * If you already have your authorization mechanism which controls when user must be redirect to initial page or logon
 * you can skip this class.
 */
@Named
@SessionScoped
@Specializes
public class LogonMB extends AdminSession implements Serializable {

    private ClientService clientservice;
    private String currentUser;
    
    private String credential;
    private String password;
    
    private String email;
    private String login;
    private String lastname;
    private String firstname;
    private String address;
    private String phone;
    
    private String passwordCheck;
    
    private boolean remember;


    public void login() throws IOException {
        clientservice = new ClientService();
        List<Client> clients = clientservice.getAllClients();
        boolean clientFound = false;
        for(Client c : clients){
            if((c.getEmail().equalsIgnoreCase(credential) || c.getLogin().equalsIgnoreCase(credential)) && c.getPassword().equals(password)){
                currentUser = c.getEmail();
                clientFound = true;
            }
        }
        if(clientFound){
            addDetailMessage("Logged in successfully as <b>" + credential + "</b>");
            Faces.getExternalContext().getFlash().setKeepMessages(true);
            Faces.redirect("index.xhtml");
        }else{
            Messages.addGlobalError("Wrong credentials");
            Faces.getExternalContext().getFlash().setKeepMessages(true);
            Faces.redirect("login.xhtml");   
        }
    }
    
    public void register() throws IOException {
        clientservice = new ClientService();
        List<Client> clients = clientservice.getAllClients();
        boolean loginExist = false;
        boolean emailExist = false;
        boolean passwordMatch = false;
        int error = 0;
        for(Client c : clients){
            if(c.getEmail().equalsIgnoreCase(email)){
                emailExist = true;
            }
            if(c.getLogin().equalsIgnoreCase(login)){
                loginExist = true;
            }
        }
        if(password.equals(passwordCheck)){
            passwordMatch = true;
        }
        
        if(loginExist){
            Messages.addGlobalError("Login already exist");
            error++;
        }
        if(emailExist){
            Messages.addGlobalError("Email already exist");
            error++;
        }
        if(!passwordMatch){
            Messages.addGlobalError("Password doesn't match");
            error++;
        }
        
        if(error>0){
            Faces.getExternalContext().getFlash().setKeepMessages(true);
            Faces.redirect("login.xhtml");
        }else{
            Client newClient = new Client();
            newClient.setLogin(login);
            newClient.setPassword(password);
            newClient.setLastname(lastname);
            newClient.setFirstname(firstname);
            newClient.setPhone(phone);
            newClient.setAddress(address);
            newClient.setEmail(email);
            
            clientservice.create(newClient);
            currentUser = email;
            addDetailMessage("Logged in successfully as <b>" + email + "</b>");
            Faces.getExternalContext().getFlash().setKeepMessages(true);
            Faces.redirect("index.xhtml");
        }
    }

    @Override
    public boolean isLoggedIn() {

        return currentUser != null;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRemember() {
        return remember;
    }

    public void setRemember(boolean remember) {
        this.remember = remember;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordCheck() {
        return passwordCheck;
    }

    public void setPasswordCheck(String passwordCheck) {
        this.passwordCheck = passwordCheck;
    }

    public ClientService getClientservice() {
        return clientservice;
    }

    public void setClientservice(ClientService clientservice) {
        this.clientservice = clientservice;
    }

    public String getCredential() {
        return credential;
    }

    public void setCredential(String credential) {
        this.credential = credential;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
}
