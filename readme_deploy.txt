Pour d�ployer :

Pour monter la base de donn�es nous utilisons Docker:
docker-compose up

Si un probl�me intervient lors de la connection � la base de donn�e, il faut changer la configuration du fichier

src/main/resources/hibernate.cfg.xml

Ligne 47 :
<property name="hibernate.connection.url">jdbc:postgresql://localhost:5432/postgres</property>
par
<property name="hibernate.connection.url">jdbc:postgresql://IP_MACHINE_DOCKER:5432/postgres</property>

la page de connection est accesible � l'addresse :
localhost:8080/mooc/login.xhtml

ID d'authentification :
login : molkiki
mdp   : testmdp

